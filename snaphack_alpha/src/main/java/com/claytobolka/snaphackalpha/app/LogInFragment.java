package com.claytobolka.snaphackalpha.app;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

/**
 * Created by tobolkac on 2/28/14.
 */
public class LogInFragment extends Fragment {

    EditText mPasswordView;
    EditText mEmailView;

    public static LogInFragment goToLogIn() {
        LogInFragment frag = new LogInFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_login, container, false);

        Button mSignUpButton = (Button) rootView.findViewById(R.id.sign_up_button);
        mSignUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSignUp();
            }
        });

        mPasswordView = (EditText) rootView.findViewById(R.id.password_login);
        mEmailView = (EditText) rootView.findViewById(R.id.username_login);

        Button mLogInButton = (Button) rootView.findViewById(R.id.log_in_button);
        mLogInButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LogInActivity)getActivity()).logInProcess(mEmailView.getText().toString(), mPasswordView.getText().toString());
            }
        });

        return rootView;
    }

    public void showSignUp(){
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.login_container, SignUpFragment.goToSignUp())
                .commit();
    }

    public LogInFragment() {
    }

    private boolean isPasswordValid(String password) {
        //TODO: Replace this with your own logic add email and username validate
        return password.length() > 4;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
