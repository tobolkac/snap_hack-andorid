package com.claytobolka.snaphackalpha.app;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by tobolkac on 2/28/14.
 */
public class SignUpFragment extends Fragment {

    EditText mUserName;
    EditText mPassword;
    EditText mEmail;

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static SignUpFragment goToSignUp() {
        SignUpFragment frag = new SignUpFragment();
        Bundle args = new Bundle();
        frag.setArguments(args);
        return frag;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_signup, container, false);

        mUserName = (EditText) rootView.findViewById(R.id.username_sign_up);
        mEmail = (EditText) rootView.findViewById(R.id.email_sign_up);
        mPassword = (EditText) rootView.findViewById(R.id.password_sign_up);

        TextView alreadyMember = (TextView) rootView.findViewById(R.id.already_member);
        alreadyMember.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showLogIn();
            }
        });

        Button signUpButton = (Button) rootView.findViewById(R.id.sign_up_sign_up_button);
        signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((LogInActivity)getActivity()).signUpProcess(mUserName.getText().toString(), mEmail.getText().toString(), mPassword.getText().toString());
            }
        });

        return rootView;
    }

    public void showLogIn(){
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.beginTransaction()
                .replace(R.id.login_container, LogInFragment.goToLogIn())
                .commit();
    }

    public SignUpFragment() {
    }



    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
