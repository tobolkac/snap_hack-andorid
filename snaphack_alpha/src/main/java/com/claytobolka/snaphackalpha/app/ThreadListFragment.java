package com.claytobolka.snaphackalpha.app;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by tobolkac on 2/28/14.
 */
public class ThreadListFragment extends Fragment {
    private ViewPager mPager;
    private PagerAdapter mPagerAdapter;
    private static final int NUM_PAGES = 2;
    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static ThreadListFragment newInstance(int sectionNumber) {
        ThreadListFragment fragment = new ThreadListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);
        return fragment;
    }

    public ThreadListFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_threads_shell, container, false);
        //ListView threads = (ListView) rootView.findViewById(R.id.threads_list);
        mPager = (ViewPager) rootView.findViewById(R.id.pager);

        mPagerAdapter = new SampleAdapter(getActivity(), getChildFragmentManager());
        mPager.setAdapter(mPagerAdapter);


        //populate list with messages

        //TODO: make different fragments for each of teh screens
        //ArrayAdapter a = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, arr);
        //threads.setAdapter(a);


        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }

    public class SampleAdapter extends FragmentPagerAdapter {
        Context ctxt=null;

        public SampleAdapter(Context ctxt, FragmentManager mgr) {
            super(mgr);
            this.ctxt=ctxt;
        }

        @Override
        public int getCount() {
            return(10);
        }

        @Override
        public Fragment getItem(int position) {
            if(position == 0)
            {
                return(ThreadListFragment.newInstance(position));
            }

            return(OtherFragment.newInstance(position));
        }


     }
}
