package com.claytobolka.snaphackalpha.app;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by tobolkac on 2/28/14.
 */
public class OtherFragment extends Fragment {

    /**
     * The fragment argument representing the section number for this
     * fragment.
     */
    private static final String ARG_SECTION_NUMBER = "section_number";

    /**
     * Returns a new instance of this fragment for the given section
     * number.
     */
    public static OtherFragment newInstance(int sectionNumber) {
        OtherFragment fragment = new OtherFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, sectionNumber);
        fragment.setArguments(args);

        return fragment;
    }

    public OtherFragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_main_other, container, false);
        //ListView threads = (ListView) rootView.findViewById(R.id.threads_list);

        //populate list with messages

        //TODO: make different fragments for each of teh screens
        //ArrayAdapter a = new ArrayAdapter(getActivity(), android.R.layout.simple_list_item_1, arr);
        //threads.setAdapter(a);


        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
    }
}
